# New Issue : n°XX
## Expected Behaviour
[...]
## Current Behaviour
[...]
## Possible Solution 
[...]
## Steps to Reproduce 
1. 
2.
3. 
## Others info
[...]

/label ~"new-issue"
/label ~"review-request"
/assign @Didou
