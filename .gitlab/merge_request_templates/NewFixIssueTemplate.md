# Fix Issue : Merge Request n°XX 
## Description of the correctif
[...]
## If related to an issue, add the link :
[...]
## Others info
[...]

/label ~"new-merge-request"
/label ~"review-request"
/assign @Didou
