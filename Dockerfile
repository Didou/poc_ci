# Base
FROM ubuntu:latest

# Author
LABEL maintainer = "Did Youn <didier.youn@gmail.com>"

# Directory SRC
WORKDIR /srv/app

# Variables
ENV NGINX_CONF  /etc/nginx/nginx.conf
ENV PHP_VERSION "7.1"
ENV PHP_CONF    /etc/php7.1/fpm/php.ini
ENV FPM_CONF    /etc/php7.1/fpm/php-fpm.conf
ENV FPM_POOL    /etc/php7.1/fpm/pool.d/www.conf

# Update, upgrade, install core packages
RUN apt-get update -y && \
	apt-get upgrade -y && \
	apt-get install -y \
	wget \
	curl \
	git \
	supervisor

# Install PHP
RUN apt-get update -y && apt-get install -y software-properties-common language-pack-en-base \
    && LC_ALL=en_US.UTF-8 add-apt-repository -y ppa:ondrej/php \
    && apt-get update -y \
    && apt-get install -y php${PHP_VERSION} \
        php${PHP_VERSION}-fpm \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-xdebug \
        php${PHP_VERSION}-mysql \
        mcrypt \
        php${PHP_VERSION}-gd \
        curl \
        php${PHP_VERSION}-curl \
        php-redis \
        php${PHP_VERSION}-mbstring

# Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    chmod a+x composer.phar && \
    mv composer.phar /usr/local/bin/composer

# Install Nginx
RUN apt-get update && apt-get install -y nginx

# Install Selenium / Chrome and Chromedriver
ADD ./docker/script/chrome.sh /chrome.sh
RUN chmod +x /chrome.sh && sh /chrome.sh

# Cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Setup folders
RUN mkdir -p /etc/nginx/servers /run/php

# Configurations
ADD ./docker/supervisor/supervisord.conf /etc/supervisord.conf
ADD ./docker/script/entrypoint.sh /entrypoint.sh
ADD ./docker/nginx/app.conf /etc/nginx/servers/app.conf
COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY . /srv/app

# Setup permissions
RUN chmod 777 /srv/app -Rf && \
    chmod +x /entrypoint.sh

# Install dependencies
RUN sh /entrypoint.sh install

# Ports
EXPOSE 80 443

# Launch services
CMD ["/usr/bin/supervisord", "-n", "-c",  "/etc/supervisord.conf"]