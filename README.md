# Continuous Integration
[![pipeline status](https://gitlab.com/Didou/poc_ci/badges/master/pipeline.svg)](https://gitlab.com/Didou/poc_ci/commits/master)
[![coverage report](https://gitlab.com/Didou/poc_ci/badges/master/coverage.svg)](https://gitlab.com/Didou/poc_ci/commits/master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
```
> docker build -t gitlab/ci .
> docker run --name gitlab_ci -p 8080:80 gitlab/ci
```

### Prerequisite

What things you need to install the software and how to install them

```
> PHP : 7 / 7.1 / 7.2
> docker, docker-compose
```

## Running the tests

We use 3 tests runners in the application :
1. Behat
2. PHPUnit
3. PHPCs

You can run them by the followings commands :
```
> composer test
> composer behat
> composer phpcs
```

### And coding style tests

```
> Symfony (YodaStyle)
```

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/Didou/poc_ci/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Didier Youn**

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
