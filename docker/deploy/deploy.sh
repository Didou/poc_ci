#!/usr/bin/env bash
#
# DESCRIPTION: ECS Deployment Script
# MAINTAINER: Did Youn <didier.youn@gmail.com>
#

# BEGIN CUSTOMIZATIONS #
ECS_REGION='us-west-2'
ECS_CLUSTER_NAME='name-of-ecs-cluster'
ECS_SERVICE_NAME='NameOfService'
ECS_TASK_DEFINITION_NAME='NameOfTaskDefinition'
ECR_NAME='name-of-ecr'
ECR_URI='account-number.dkr.ecr.us-west-2.amazonaws.com'
VERSION=$(date +%s)
AWSCLI_VER_TAR=1.11.91
# END CUSTOMIZATIONS #

# Install Docker

# Pull image from registry

# Run im