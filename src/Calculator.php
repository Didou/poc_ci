<?php

namespace App;

/**
 * Class Calculator
 *
 * @package App
 * @author          Didier Youn <didier.youn@dnd.fr>
 * @copyright       Copyright (c) 2017 Agence Dn'D
 * @license         http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link            http://www.dnd.fr/
 */
class Calculator
{
    /**
     * Addition
     *
     * @param float $a
     * @param float $b
     * @return float
     */
    public function addition(float $a, float $b) : float
    {
        return $a + $b;
    }

    /**
     * Substraction
     *
     * @param float $a
     * @param float $b
     * @return float
     */
    public function substraction(float $a, float $b) : float
    {
        return $a - $b;
    }

    /**
     * Division
     *
     * @param float $a
     * @param float $b
     *
     * @return float
     * @throws CalculatorException
     */
    public function division(float $a, float $b) : float
    {
        if (0 === $b) {
            throw new CalculatorException('Division by zero');
        }

        $result = $a / $b;

        return $result;
    }
}