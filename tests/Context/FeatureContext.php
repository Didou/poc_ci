<?php

namespace AppTest\Context;

use SensioLabs\Behat\PageObjectExtension\Context\PageObjectContext;

use Behat\Behat\Context\Context;

/**
 * Class FeatureContext
 *
 * @author          Didier Youn <didier.youn@dnd.fr>
 * @copyright       Copyright (c) 2018 Agence Dn'D
 * @license         http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link            http://www.dnd.fr/
 */
class FeatureContext implements Context
{
}