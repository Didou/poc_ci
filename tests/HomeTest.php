<?php

namespace AppTest;

use App\Calculator;
use Behat\MinkExtension\Context\MinkContext;
use PHPUnit\Framework\TestCase;

/**
 * Class HomeTest
 *
 * @package         App
 * @author          Didier Youn <didier.youn@dnd.fr>
 * @copyright       Copyright (c) 2017 Agence Dn'D
 * @license         http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link            http://www.dnd.fr/
 */
class HomeTest extends MinkContext
{

}