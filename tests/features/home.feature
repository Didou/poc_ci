Feature: Check page response
  In order to navigate
  As an user
  I need to go on every page I want

  Scenario: Going on homepage
    When I go to "/"
    Then I should see "Home"