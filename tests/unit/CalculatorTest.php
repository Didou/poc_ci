<?php

namespace AppTest\unit;

use App\Calculator;
use App\CalculatorException;
use PHPUnit\Framework\TestCase;

/**
 * Class CalculatorTest
 *
 * @package         App
 * @author          Didier Youn <didier.youn@dnd.fr>
 * @copyright       Copyright (c) 2017 Agence Dn'D
 * @license         http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link            http://www.dnd.fr/
 */
class CalculatorTest extends TestCase
{
    /** @var Calculator */
    private $calculator;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        if (is_null($this->calculator)) {
            $this->calculator = new Calculator();
        }
    }

    /**
     * Test addition
     *
     * @covers \App\Calculator::addition()
     */
    public function testAddition()
    {
        $this->assertEquals(5,$this->calculator->addition(2.5, 2.5));
        $this->assertNotEquals(7,$this->calculator->addition(2.5, 2.5));
    }

    /**
     * Test substraction
     *
     * @covers \App\Calculator::substraction()
     */
    public function testSubstraction()
    {
        $this->assertEquals(10,$this->calculator->substraction(20, 10));
        $this->assertEquals(15,$this->calculator->substraction(20, 5));
    }

    /**
     * @covers \App\Calculator::division()
     * @throws CalculatorException
     */
    public function testDivision()
    {
        $this->assertEquals(5, $this->calculator->division(20, 4));
    }


    /**
     * {@inheritdoc}
     */
    public function tearDown()
    {
        //
    }
}